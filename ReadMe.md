# Liches-CR

Lichess-CR is a crystal library for validating chess movement. It is largely just meant as library for Lisera,
the Lichess bot middleware client though I'm sure it could have other uses.

## Usage

To use this client, simply clone this repo / download it, and add your bot into the `engines` folder.
As of now, you are required to have Crystal `>= 1.2.1` installed.

`git clone https://gitlab.com/jxno/lisera.git`

Currently only UCI bots are supported.

## Development

`git clone https://gitlab.com/jxno/lisera.git`

`shards build`

`crystal docs` <- To view the full documentation

And then get to work in your editor of choice.

## Contributing

1. Fork it (<https://gitlab.com/jxno/lisera/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Jxno](https://gitlab.com/jxno) - creator and maintainer

