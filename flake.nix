{
  description = "Crystal port of the chess PyPi package.";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-20.03;
  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.hello = nixpkgs.legacyPackages.x86_64-linux.hello;

    defaultPackage.aarch64-darwin =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "aarch64-darwin"; };
      stdenv.mkDerivation {
        name = "lichess-cr";
        src = self;
        buildPhase = "shards build --release";
        installPhase = ".";
      };

    defaultPackage.x86_64-linux =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "lichess-cr";
        src = self;
        buildPhase = "shards build --release";
        installPhase = "mkdir -p $out/bin; install -t $out/bin hello";
      };

  };
}
